class Article < ApplicationRecord

	validates :title, :presence => {message: "no puede estar vacio"}, 
				length: { minimum: 5, message: "minimo 5 caracteres para el título" }
    validates :text, :presence => {message: "no puede estar vacio"}, 
    			length: { maximum: 100, message: "maximo 100 caracteres para el resumen"}        

    has_attached_file :cover
    validates_attachment_content_type :cover, content_type: ['application/pdf']                  
end
