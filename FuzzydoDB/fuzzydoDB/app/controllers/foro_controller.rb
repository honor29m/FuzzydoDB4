class ForoController < ApplicationController

  def index
  	if !current_user
  		redirect_to "http://127.0.0.1:3000/foro/member/-1"
  	else
  		redirect_to "http://127.0.0.1:3000/foro/member/"+current_user.id.to_s
  	end
  end

end