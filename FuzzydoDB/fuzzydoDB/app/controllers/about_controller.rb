class AboutController < ApplicationController

  def index
    @title = "about-us.introduction.title"
    @content = "about-us.introduction.content"
    @active_item = "introduction"

    render "about_us_plain"
  end

  def history
    @title = "about-us.history.title"
    @content = "about-us.history.content"
    @active_item = "history"

    render "about_us_plain"
  end

  def members
    @members = Member.get_non_developers()
    #@developers = Developer.all
  end

  def member
    @member = Member.find(params[:id])
    if @member.nil?
      @error = "errors.not_found"
      render(:file => File.join(Rails.root, 'app/views/shared/error.html'), :status => 404, :layout => false)
    end
  end

  def dev
    @member = Developer.find(params[:id])
    if @member.nil?
      @error = "errors.not_found"
      render(:file => File.join(Rails.root, 'app/views/shared/error.html'), :status => 404, :layout => false)
    end
  end

end
