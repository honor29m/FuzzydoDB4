class HomeController < ApplicationController
  def show
  end

  def index
  	if !current_user
  		redirect_to "http://127.0.0.1:4000/home/member/-1"
  	else
  		redirect_to "http://127.0.0.1:4000/home/member/"+current_user.id.to_s
  	end
  end
end
