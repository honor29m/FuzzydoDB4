## frozen_string_literal: true
admin = User.create!(
    display_name: 'Admin',
    email: "admin@forofuzzydodb.com",
    password: "123456",
    admin: true
)

oskcolorado = User.create!(
    display_name: 'Oskcolorado',
    email: "lcolorado7@gmail.com",
    password: "123456",
    admin: true
)

Developer.create!(member_id: "campos",
                  name: "Marcos Campos",
                  email: "10-10108@usb.ve")

Developer.create!(member_id: "delgado",
                  name: "John Delgado",
                  email: "10-10196@usb.ve")

Developer.create!(member_id: "colorado",
                  name: "Luis Colorado",
                  email: "09-11086@usb.ve")

Developer.create!(member_id: "pascarella",
                  name: "Jose Pascarella",
                  email: "11-10743@usb.ve")

Developer.create!(member_id: "ng",
                  name: "Chi Seng Ng",
                  email: "09-11245@usb.ve")

Member.create!(member_id: "rodriguez",
               name: "Rosseline Rodríguez",
               email: "crodig@usb.ve",
               photo: "rosseline.png")

Member.create!(member_id: "tineo",
               name: "Leonid Tineo",
               email: "leonid@usb.ve",
               photo: "leonid.jpg")

Member.create!(member_id: "carrasquel",
               name: "Soraya Carrasquel",
               email: "scarrasquel@usb.ve",
               articles: [Article.create!(authors: "Carrasquel, Soraya; Rodriguez, Rosseline; Tineo, L.",
                  description: "\"La mujer computista: Presencia e influencia en su división dentro de la USB\". Novática. 2015. Indexada en el Latindex Catálogo. Artículo Invitado. . Vol. 231, pp. 53 - 62."),
                  Article.create!(authors: "Rodriguez, Rosseline; Tineo, L; Carrasquel, Soraya.",
                  description: "\"Consultas con agrupamiento basado en similitud \". Ingeniare, Revista Chilena de Ingeniería. 2014. Indexada en el Scopus, Latindex Catálogo, Scielo. . Vol. 22, pp. 517 - 527.")])

Member.create!(member_id: "coronado",
               name: "David Coronado",
               email: "dcoronado@usb.ve",
               photo: "david.jpg")

Member.create!(member_id: "cadenas",
               name: "José Cadenas",
               email: "jtcadenas@usb.ve")

Member.create!(member_id: "ramirez",
               name: "Josué Ramírez",
               email: "ramirezjosue@usb.ve")

Member.create!(member_id: "monascal",
               name: "Ricardo Monascal",
               email: "rmonascal@usb.ve",
               photo: "monascal.jpg")

Member.create!(member_id: "rocha",
               name: "Darwin Rocha",
               email: "darwinrocha@usb.ve",
               photo: "darwin.png")

User.create!(name: "Marcos Campos",
            display_name: "Marcos Campos",
            username: "whosthemark",
            email: "whosthemark@gmail.com",
            password: "123123",
            role: :member)

User.create!(name: "John Delgado",
            display_name: "John Delgado",
            username: "pexison",
            email: "pexison@gmail.com",
            password: "123123",
            role: :member)

User.create!(name: "Soraya Carrasquel",
            display_name: "Soraya Carrasquel",
            username: "scarrasquel",
            email: "scarrasquel@usb.ve",
            password: "123123",
            role: :member)

User.create!(name: "David Coronado",
            display_name: "David Coronado",
            username: "dcoronado",
            email: "dcoronado@usb.ve",
            password: "123123",
            role: :member)

User.create!(name: "Rosseline Rodríguez",
            display_name: "Rosseline Rodríguez",
            username: "rrodriguez",
            email: "rrodriguez@usb.ve",
            password: "123123",
            role: :member)

User.create!(name: "Super Miembro",
            display_name: "Super Miembro",
            username: "Super",
            email: "super@fuzzydo.db",
            password: "123123",
            role: :super_member)

messageboard = Thredded::Messageboard.create!(
    name: 'General',
    slug: 'general',
    description: 'A board is not a board without some posts'
)

Thredded::TopicForm.new(
    title: 'My first topic',
    content: <<~MARKDOWN,
Hello **world**! :smile: This first post shows some of the Thredded default post
formatting functionality.

### Quote

> There is nothing either good or bad, but thinking makes it so.

### Image

![lime-cat](https://pymex.pe/wp-content/uploads/2016/09/administrador.jpg)

### Video

https://youtu.be/RogZileC1XE

### Table

| x | y | x ⊕ y |
|---|---|:-----:|
| 1 | 1 |   0   |
| 1 | 0 |   1   |
| 0 | 1 |   1   |
| 0 | 0 |   0   |

### Code

```ruby
puts 'Hello world'
```

Code highlighting can be enabled by installing the
[Markdown Coderay plugin](https://github.com/thredded/thredded-markdown_coderay).

BBCode support (e.g. [b]bold[/b]) can be enabled by installing the
[BBCode plugin](https://github.com/thredded/thredded-bbcode).

TeX Math support (e.g. $$phi$$) can be enabled by installing the
[KaTeX plugin](https://github.com/thredded/thredded-markdown_katex).
    MARKDOWN
    user: admin,
    messageboard: messageboard
).save
