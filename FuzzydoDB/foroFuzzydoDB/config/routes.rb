Rails.application.routes.draw do
	scope '/(:locale)', defaults: { locale: 'es' }, constraints: { locale: /en|es|pt-br/ } do
		root to: 'home#index'
		devise_for :users
		get 'foro' => 'foro#index'

		scope 'foro' do
	      get 'member/:id' => 'foro#member'
	    end
		# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
		resources :users, only: [:show]
		mount Thredded::Engine => '/forum'
	end
end
