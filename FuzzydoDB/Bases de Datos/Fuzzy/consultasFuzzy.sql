/*create fuzzy domain nombre as values('e1','e2', 'e3') similarity { ('e1', 'e2')/1.0, ('e1', 'e3')/0.5 }
create table tabla1 as values(atributo1 integer, atributo2 nombre, atributo3 TEXT)
insert into tabla1 values(1,'e1', "hola")
*/

create fuzzy domain deporte_jugadores as values('Futbol','Beisbol','Baloncesto','Tenis','Natacion','Waterpolo','Voleibol') similarity {('Futbol','Beisbol')/0.4,('Futbol','Baloncesto')/0.6,('Futbol','Waterpolo')/0.7,('Futbol','Voleibol')/0.5,('Beisbol','Baloncesto')/0.5,('Beisbol','Tenis')/0.1,('Baloncesto','Waterpolo')/0.2,('Baloncesto','Voleibol')/0.7,('Tenis','Voleibol')/0.3,('Natacion','Waterpolo')/0.6,('Waterpolo','Voleibol')/0.5};

--create table deportes(id Integer,value deporte_jugadores);

-- no acepta las etiquetas como string dado de que los convierte en numeros
-- insert into deportes values(1,1);


--funciona pero da el error del ifnull
select * from test_sports.deportistas as t, deportes as d where t.deporte=d.value order by similarity on value start 'Tenis';
select * from test_sports.deportistas as t, deportes as d where t.deporte=d.value order by value starting from 'Tenis';
CREATE TABLE IF NOT EXISTS test_sports.deportistas (codigo_jugador VARCHAR(8) NOT NULL PRIMARY KEY, nombre VARCHAR(64) NOT NULL, fecha_nac DATE NOT NULL, altura NUMERIC(3) NOT NULL, sexo CHAR NOT NULL CHECK (sexo IN ('F','M')), pais VARCHAR(32) NOT NULL, deporte deporte_jugadores NOT NULL);

--forma de correr un script
java -jar FuzzyDB/dist/FuzzyDB.jar < script-que-quieran.sql

--correr en postgres
SELECT S1.value, deportistas.codigo_jugador, deportistas.nombre, deportistas.fecha_nac, deportistas.altura, deportistas.sexo, deportistas.pais, L1.label_name AS deporte 
FROM ((deportistas LEFT JOIN information_schema_fuzzy.labels AS L1 ON deportistas.deporte = L1.label_id) LEFT JOIN information_schema_fuzzy.similarities AS S1 ON (S1.label1_id = (SELECT label_id FROM information_schema_fuzzy.labels AS L2 WHERE L2.label_name = 'Waterpolo' AND L2.domain_id = (SELECT D1.domain_id FROM information_schema_fuzzy.domains AS D1 WHERE D1.domain_name = 'tipo_deportes')) AND S1.label2_id = deportistas.deporte))
ORDER BY S1.value desc;