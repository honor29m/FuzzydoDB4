--consulta 1
select nombre, sexo, deporte
from test_sports.deportistas;
--lo que devuelve FuzzyDB
SELECT nombre, sexo, L1.label_name AS deporte
FROM (test_sports.deportistas LEFT JOIN information_schema_fuzzy.labels AS L1 ON test_sports.deportistas.deporte = L1.label_id);

--consulta 1 traducida
select nombre, sexo, l_1.label_name as deporte
from test_sports.deportistas as t_1 left join information_schema_fuzzy.labels as l_1 on (t_1.deporte = l_1.label_id);

--consulta 2
select distinct deporte
from test_sports.deportistas;
--lo que devuelve FuzzyDB
SELECT DISTINCT L1.label_name AS deporte
FROM (test_sports.deportistas LEFT JOIN information_schema_fuzzy.labels AS L1 ON test_sports.deportistas.deporte = L1.label_id);

--consulta 2 traducida
select distinct l_1.label_name as deporte
from test_sports.deportistas as t_1 left join information_schema_fuzzy.labels as l_1 on (t_1.deporte = l_1.label_id);

--consulta 3
select distinct deporte
from test_sports.deportistas
order by deporte;
--lo que devuelve FuzzyDB
SELECT DISTINCT L1.label_name AS deporte
FROM (test_sports.deportistas LEFT JOIN information_schema_fuzzy.labels AS L1 ON test_sports.deportistas.deporte = L1.label_id)
ORDER BY L1.label_name ASC;

--consulta 3 traducida (NO CORRE)
select distinct l_1.label_name as deporte
from test_sports.deportistas as t_1 left join information_schema_fuzzy.labels as l_1 on (t_1.deporte = l_1.label_id)
order by l_1.label_name asc;
--consulta 3 traducida (NO CORRE)
select distinct l_1.label_name as deporte from test_sports.deportistas as t_1 left join information_schema_fuzzy.labels as l_1 on (t_1.deporte = l_1.label_id) order by deporte asc;

--consulta 4
select *
from test_sports.deportistas
where nombre like 'R%' and deporte in ('Futbol', 'Tenis') or deporte = 'Beisbol';
--lo que devuelve fuzzy
SELECT test_sports.deportistas.codigo_jugador, test_sports.deportistas.nombre, test_sports.deportistas.fecha_nac, test_sports.deportistas.altura, test_sports.deportistas.sexo, test_sports.deportistas.pais, L1.label_name AS deporte
FROM (test_sports.deportistas LEFT JOIN information_schema_fuzzy.labels AS L1 ON test_sports.deportistas.deporte = L1.label_id)
WHERE nombre LIKE 'R%' AND L1.label_name IN ('Futbol', 'Tenis') OR L1.label_name = 'Beisbol';

--consulta 4 traducida
select codigo_jugador, nombre, fecha_nac, altura, sexo, pais, l_1.label_name as deporte
from test_sports.deportistas as t_1 left join information_schema_fuzzy.labels as l_1 on (t_1.deporte = l_1.label_id)
where nombre like 'R%' and l_1.label_name in ('Futbol','Tenis')or l_1.label_name = 'Beisbol';

--consulta 5 (NO Corre)
select nombre, sexo, deporte
from test_sports.deportistas
order by similarity on deporte starting from 'Futbol';

--consulta 5 (corre)
select nombre, sexo, deporte
from test_sports.deportistas
order by deporte asc;

--consulta 5 traducida (NO CORRE)
select t_1.nombre, t_1.sexo, l_1.label_name as deporte, l_2.label_name, S1.value
from test_sports.deportistas as t_1 left join information_schema_fuzzy.labels as l_1 on (t_1.deporte = l_1.label_id) left join information_schema_fuzzy.domains as d_1 on (d_1.domain_name='deporte') left join information_schema_fuzzy.labels as l_2 on (l_2.label_name = 'Futbol' and l_2.domain_id = d_1.domain_id) left join information_schema_fuzzy.similarities S1 on (S1.label1_id = l_2.label_id and S1.label2_id = t_1.deporte)
where S1.value is not null
order by S1.value desc;

--consulta 5 traducida (corre)
select nombre,sexo, l_1.label_name as deporte from test_sports.deportistas as t_1 left join information_schema_fuzzy.labels as l_1 on (t_1.deporte = l_1.label_id) order by deporte asc;

--consulta 6
select *
from test_sports.deportistas
order by nombre;
--lo que devuelve fuzzy
SELECT test_sports.deportistas.codigo_jugador, test_sports.deportistas.nombre, test_sports.deportistas.fecha_nac, test_sports.deportistas.altura, test_sports.deportistas.sexo, test_sports.deportistas.pais, L1.label_name AS deporte
FROM (test_sports.deportistas LEFT JOIN information_schema_fuzzy.labels AS L1 ON test_sports.deportistas.deporte = L1.label_id)
ORDER BY nombre ASC;

--consulta 6 traducida
select codigo_jugador, nombre, fecha_nac, altura, sexo, pais, l_1.label_name as deporte
from test_sports.deportistas as t_1 left join information_schema_fuzzy.labels as l_1 on (t_1.deporte = l_1.label_id)
order by nombre asc;

--consulta 7
select *
from test_sports.deportistas
order by deporte asc;
--lo que devuelve fuzzy
SELECT test_sports.deportistas.codigo_jugador, test_sports.deportistas.nombre, test_sports.deportistas.fecha_nac, test_sports.deportistas.altura, test_sports.deportistas.sexo, test_sports.deportistas.pais, L1.label_name AS deporte
FROM (test_sports.deportistas LEFT JOIN information_schema_fuzzy.labels AS L1 ON test_sports.deportistas.deporte = L1.label_id)
ORDER BY L1.label_name ASC;

--consulta 7 traducida
select codigo_jugador, nombre, fecha_nac, altura, sexo, pais, l_1.label_name as deporte
from test_sports.deportistas as t_1 left join information_schema_fuzzy.labels as l_1 on (t_1.deporte = l_1.label_id)
order by deporte asc;

