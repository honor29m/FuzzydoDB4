
-- el nombre de la prueba debe ser todo en minusculas
CREATE SCHEMA IF NOT EXISTS test_sports; -- CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS test_sports.deportistas (
  codigo_jugador VARCHAR(8) NOT NULL PRIMARY KEY,
  nombre VARCHAR(64) NOT NULL,
  fecha_nac DATE NOT NULL,
  altura NUMERIC(3) NOT NULL, -- en centimetros es mejor.
  sexo CHAR NOT NULL CHECK (sexo IN ('F','M')),
  pais VARCHAR(32) NOT NULL,
  deporte INTEGER NOT NULL REFERENCES information_schema_fuzzy.labels (label_id) ON UPDATE CASCADE ON DELETE CASCADE);

--CHECK (deporte IN ('Beisbol', 'Futbol', 'Baloncesto', 'Tenis', 'Natacion', 'Waterpolo', 'Voleibol'))

--select TD.codigo_jugador, TD.nombre, TD.deporte from test_sports.deportistas as TD left join information_schema_fuzzy.labels as SF on (TD.domain_name = 'deporte') left join information_schema_fuzzy.labels as SF2 on (SF2.label_name = 'Futbol' and SF2.domain_id = SF.domain_id) left join information_schema_similarities as S1 on (S1.label1_id = SF2.label_id and s1.label2_id = TD.deporte) where S1.value is not NULL order by S1.value desc;
