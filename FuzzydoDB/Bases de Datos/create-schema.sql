-- El Dominio difuso tipo 2 poderes_base_pokemon nos muestra un estimado
-- del poder de un pokemon tomando como base sus estadisticas base puntuales
-- de ataque, defensa, ataque especial y defensa especial.  
CREATE FUZZY DOMAIN poderes_base_pokemon AS POSSIBILITY DISTRIBUTION ON INTEGER;

-- EL primer Dominio difuso tipo 3 tipos_pokemon nos muestra un estimado
-- de una relacion de similitud entre los 18 tipos elementales del
-- videojuego pokemon. 
CREATE FUZZY DOMAIN tipos_pokemon AS VALUES ('Tipo planta','Tipo fuego','Tipo agua','Tipo electrico','Tipo bicho','Tipo normal','Tipo volador','Tipo lucha','Tipo veneno','Tipo tierra','Tipo roca','Tipo acero','Tipo dragon','Tipo fantasma','Tipo hielo','Tipo psiquico','Tipo siniestro','Tipo hada') SIMILARITY {('Tipo planta','Tipo fuego')/0.1, ('Tipo planta','Tipo agua')/0.5, ('Tipo planta','Tipo bicho')/0.7, ('Tipo planta','Tipo veneno')/0.2, ('Tipo planta','Tipo tierra')/0.9, ('Tipo fuego','Tipo electrico')/0.6, ('Tipo fuego','Tipo acero')/0.3,  ('Tipo fuego','Tipo dragon')/0.8, ('Tipo fuego','Tipo hielo')/0.2, ('Tipo agua','Tipo electrico')/0.8, ('Tipo agua','Tipo tierra')/0.1,  ('Tipo agua','Tipo roca')/0.3, ('Tipo agua','Tipo hielo')/0.9, ('Tipo electrico','Tipo dragon')/0.3, ('Tipo bicho','Tipo volador')/0.6,  ('Tipo bicho','Tipo veneno')/0.4, ('Tipo bicho','Tipo tierra')/0.3, ('Tipo bicho','Tipo psiquico')/0.2, ('Tipo normal','Tipo lucha')/0.4,  ('Tipo volador','Tipo dragon')/0.9, ('Tipo volador','Tipo fantasma')/0.7, ('Tipo volador','Tipo psiquico')/0.2, ('Tipo volador','Tipo hada')/0.8, ('Tipo lucha','Tipo roca')/0.5, ('Tipo lucha','Tipo acero')/0.6, ('Tipo lucha','Tipo psiquico')/0.3, ('Tipo veneno','Tipo siniestro')/0.7, ('Tipo tierra','Tipo roca')/0.9, ('Tipo tierra','Tipo acero')/0.1, ('Tipo acero','Tipo psiquico')/0.1, ('Tipo dragon','Tipo siniestro')/0.3, ('Tipo dragon','Tipo hada')/0.5, ('Tipo fantasma','Tipo psiquico')/0.5, ('Tipo fantasma','Tipo siniestro')/0.9, ('Tipo psiquico','Tipo siniestro')/0.3};

-- EL segundo Dominio difuso tipo 3 grupos_huevo_pokemon nos muestra un estimado
-- de una relacion de similitud entre los 15 grupos huevo del
-- videojuego pokemon. 
CREATE FUZZY DOMAIN grupos_huevo_pokemon AS VALUES('Ninguno','Ditto','Planta','Bicho','Volador','Humanoide','Amorfo','Mineral','Campo','Agua 1','Agua 2','Agua 3','Monstruo','Hada','Dragon') SIMILARITY {('Ninguno','Ditto')/1,('Planta','Bicho')/0.7,('Planta','Mineral')/0.5, ('Planta','Campo')/0.8,('Bicho','Volador')/0.6, ('Bicho','Campo')/0.7,('Volador','Hada')/0.8, ('Volador','Dragon')/0.9,('Amorfo','Mineral')/0.3, ('Amorfo','Monstruo')/0.8,('Mineral','Campo')/0.6, ('Agua 1','Agua 2')/0.9,('Agua 1','Agua 3')/0.8, ('Agua 2','Agua 3')/0.9,('Monstruo','Dragon')/0.6, ('Hada','Dragon')/0.5};

-- La Tabla pokemon esta basada en un famoso videojuego con este mismo
-- nombre. Los atributos que presenta esta tabla son los siguientes:
-- El codigo_pokedex_nacional hace referencia al numero del pokemon
-- desde el punto de vista de la pokedex nacional. El nombre se
-- refiere al nombre del pokemon. El tipo_1 y el tipo_2 hacen referencia
-- a los tipos de los pokemon, siendo el tipo_1 el tipo primario del 
-- pokemon y el tipo_2 su tipo secundario. El grupo_huevo_1 y el 
-- grupo_huevo_2 hacen referencia a los grupos en los que un pokemon
-- puede tener crias. El poder_base_estimado hace referencia al poder
-- que puede tener un pokemon en cuanto a ataque, ataque especial,
-- defensa y defensa especial.
CREATE TABLE pokemon (codigo_pokedex_nacional varchar not null primary key, nombre varchar not null, tipo_1 tipos_pokemon not null, tipo_2 tipos_pokemon, grupo_huevo_1 grupos_huevo_pokemon not null, grupo_huevo_2 grupos_huevo_pokemon, poder_base_estimado poderes_base_pokemon not null);

