CREATE FUZZY DOMAIN tipos_pokemon1 AS VALUES ('Planta','Fuego','Agua','Electrico') SIMILARITY {('Planta','Fuego')/0.4,('Planta','Agua')/0.7,('Fuego','Electrico')/0.8,('Agua','Electrico')/0.6};

CREATE FUZZY DOMAIN alturas_pokemon AS POSSIBILITY DISTRIBUTION ON INTEGER;

CREATE TABLE pokemon1 (nombre varchar not null primary key, altura alturas_pokemon, tipo tipos_pokemon1);

INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Squirtle', {f(10, 15, 35, 50)}, 1);
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Blastoise', {f(90, 120, 150, 180)}, 1);
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Pikachu', {f(5, 15, 25, 35)}, 2);
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Pichu', {f(5, 10, 15, 20)}, 2);
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Charmander', {f (20, 30, 50, 60)}, 3);
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Charizard', {f (100, 130, 170, 200)}, 3);
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Bulbasaur', {f (10, 20, 30, 40)}, 4);
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Venusaur', {f (80, 100, 140, 160)}, 4);

INSERT INTO pokemon1(nombre, altura, tipo) VALUES ('Squirtle', {f(10, 15, 35, 50)}, 'Agua');
INSERT INTO pokemon1(nombre, altura, tipo) VALUES ('Blastoise', {f(90, 120, 150, 180)}, 'Agua');
INSERT INTO pokemon1(nombre, altura, tipo) VALUES ('Pikachu', {f(5, 15, 25, 35)}, 'Electrico');
INSERT INTO pokemon1(nombre, altura, tipo) VALUES ('Pichu', {f(5, 10, 15, 20)}, 'Electrico');
INSERT INTO pokemon1(nombre, altura, tipo) VALUES ('Charmander', {f (20, 30, 50, 60)}, 'Fuego');
INSERT INTO pokemon1(nombre, altura, tipo) VALUES ('Charizard', {f (100, 130, 170, 200)}, 'Fuego');
INSERT INTO pokemon1(nombre, altura, tipo) VALUES ('Bulbasaur', {f (10, 20, 30, 40)}, 'Planta');
INSERT INTO pokemon1(nombre, altura, tipo) VALUES ('Venusaur', {f (80, 100, 140, 160)}, 'Planta');

