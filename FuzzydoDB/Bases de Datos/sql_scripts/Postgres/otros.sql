--15
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Manel Estiarte', to_date('26-10-1961', 'DD-MM-YYYY'), 176, 'M', 'España', 'Waterpolo');
--18
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Tony Azevedo', to_date('21-11-1981', 'DD-MM-YYYY'), 185, 'M', 'Brasil', 'Waterpolo');
--16
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Maggie Steffens', to_date('04-06-1993', 'DD-MM-YYYY'), 175, 'F', 'Estados Unidos', 'Waterpolo');
--17
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Brenda Villa', to_date('18-04-1980', 'DD-MM-YYYY'), 163, 'F', 'Estados Unidos', 'Waterpolo');
--19
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Elizabeth Armstrong', to_date('31-01-1983', 'DD-MM-YYYY'), 188, 'F', 'Estados Unidos', 'Waterpolo');
--20
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Rowena Webster', to_date('27-01-1987', 'DD-MM-YYYY'), 178, 'F', 'Australia', 'Waterpolo');



--1
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Novak Djokovic', to_date('22-05-1987', 'DD-MM-YYYY'), 188, 'M', 'Yugoslavia', 'Tenis');
--2
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Rafael Nadal', to_date('03-06-1986', 'DD-MM-YYYY'), 185, 'M', 'España', 'Tenis');
--3
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Roger Federer', to_date('08-08-1981', 'DD-MM-YYYY'), 185, 'M', 'Suiza', 'Tenis');
--4
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Stan Wawrinka', to_date('28-03-1985', 'DD-MM-YYYY'), 183, 'M', 'Suiza', 'Tenis');
--5
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Key Nishikori', to_date('29-12-1989', 'DD-MM-YYYY'), 178, 'M', 'Japon', 'Tenis');
--6
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Juan Del Potro', to_date('23-09-1988', 'DD-MM-YYYY'), 198, 'M', 'Argentina', 'Tenis');
--7
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Simona Halep', to_date('27-09-1991', 'DD-MM-YYYY'), 168, 'F', 'Romania', 'Tenis');
--8
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Maria Sharapova', to_date('19-04-1987', 'DD-MM-YYYY'), 188, 'F', 'Rusia', 'Tenis');
--9
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Ana Ivanovic', to_date('06-11-1987', 'DD-MM-YYYY'), 184, 'F', 'Serbia', 'Tenis');
--10
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Angelique Kerber', to_date('18-06-1988', 'DD-MM-YYYY'), 173, 'F', 'Alemania', 'Tenis');
--11
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Eugenie Bouchard', to_date('25-02-1994', 'DD-MM-YYYY'), 178, 'F', 'Canada', 'Tenis');


Natacion

--41
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Michael Phelps', to_date('30-06-1985', 'DD-MM-YYYY'), 193, 'M', 'Estados Unidos', 'Natacion');
--42
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Ian Thorpe', to_date('13-10-1982', 'DD-MM-YYYY'), 195, 'M', 'Australia', 'Natacion');
--43
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Aleksandr Popov', to_date('16-11-1971', 'DD-MM-YYYY'), 200, 'M', 'Rusia', 'Natacion');
--44
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Pieter van den Hoogenband', to_date('14-03-1978', 'DD-MM-YYYY'), 193, 'M', 'Holanda', 'Natacion');
--45
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Grant Hackett', to_date('09-05-1980', 'DD-MM-YYYY'), 198, 'M', 'Australia', 'Natacion');
--46
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Ryan Steven Lochte', to_date('03-08-1984', 'DD-MM-YYYY'), 188, 'M', 'Estados Unidos', 'Natacion');
--47
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Matt Biondi', to_date('08-10-1965', 'DD-MM-YYYY'), 201, 'M', 'Estados Unidos', 'Natacion');


--46
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Krisztina Egerszegi', to_date('16-08-1974', 'DD-MM-YYYY'), 174, 'F', 'Hungria', 'Natacion');
--46
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Jennifer Beth Thompson', to_date('26-02-1973', 'DD-MM-YYYY'), 178, 'F', 'Estados Unidos', 'Natacion');
--46
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Amy Van Dyken', to_date('15-02-1973', 'DD-MM-YYYY'), 183, 'F', 'Estados Unidos', 'Natacion');
--46
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Inge de Bruijn', to_date('24-08-1973', 'DD-MM-YYYY'), 178, 'F', '', 'Natacion');
--46
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Yana Klochkova', to_date('07-08-1982', 'DD-MM-YYYY'), 182, 'F', 'Ucrania', 'Natacion');


VOLEIBOL
--53
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Dante Amaral', to_date('30-09-1980', 'DD-MM-YYYY'), 201, 'M', 'Brasil', 'Voleibol');
--54
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Gilberto Filho', to_date('23-12-1976', 'DD-MM-YYYY'), 192, 'M', 'Brasil', 'Voleibol');
--55
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Hector Soto', to_date('20-06-1978', 'DD-MM-YYYY'), 197, 'M', 'Puerto Rico', 'Voleibol');
--56
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Clayton Stanley', to_date('20-01-1978', 'DD-MM-YYYY'), 205, 'M', 'Estados Unidos', 'Voleibol');
--57
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Angela Leyva', to_date('22-11-1996', 'DD-MM-YYYY'), 182, 'F', 'Peru', 'Voleibol');
--58
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Susana Rodriguez', to_date('16-08-1976', 'DD-MM-YYYY'), 187, 'F', 'España', 'Voleibol');
--59
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Aurea Cruz', to_date('10-01-1982', 'DD-MM-YYYY'), 180, 'F', 'Puerto Rico', 'Voleibol');
--60
INSERT INTO test_Sports.deportistas(nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('Gabriela Perez', to_date('10-07-1968', 'DD-MM-YYYY'), 194, 'F', 'Peru', 'Voleibol');


WATERPOLO 

http://es.wikipedia.org/wiki/Manel_Estiarte

http://prezi.com/rbfxg3oo6rtk/top-5-female-olympic-water-polo-players/

http://es.wikipedia.org/wiki/Elizabeth_Armstrong

http://es.wikipedia.org/wiki/Tony_Azevedo

http://es.wikipedia.org/wiki/Rowena_Webster

TENIS

http://www.atpworldtour.com/Rankings/Singles.aspx

http://www.wtatennis.com/rankings

Natacion

http://es.wikipedia.org/wiki/Michael_Phelps

http://entertainmentaroundtheworld.wordpress.com/my-lists/top-10-swimmers-of-all-time/

http://en.wikipedia.org/wiki/List_of_top_Olympic_swimming_medalists

Voleibol

http://es.wikipedia.org/wiki/Dante_Amaral

http://en.wikipedia.org/wiki/Giba

http://www.fivbheroes.com/heroes/

http://es.slideshare.net/academiacristorey/los-10-atletas-mas-destacados-en-volleyball-7417443
