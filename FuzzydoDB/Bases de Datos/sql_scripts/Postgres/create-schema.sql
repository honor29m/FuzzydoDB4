CREATE SCHEMA IF NOT EXISTS test_Sports; -- CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS test_Sports.deportistas (
  codigo_jugador VARCHAR(8) NOT NULL PRIMARY KEY,
  nombre VARCHAR(64) NOT NULL,
  fecha_nac DATE NOT NULL,
  altura NUMERIC(3) NOT NULL, -- en centimetros es mejor.
  sexo CHAR NOT NULL CHECK (sexo IN ('F','M')),
  pais VARCHAR(32) NOT NULL,
  deporte VARCHAR(32) NOT NULL CHECK (deporte IN ('Beisbol', 'Futbol', 'Baloncesto', 'Tenis', 'Natacion', 'Waterpolo', 'Voleibol'))
);

