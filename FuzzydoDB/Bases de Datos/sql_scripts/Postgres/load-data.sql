-- Futbol Masculino
--1
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0001', 'Iker Casillas', to_date('20-05-1981', 'DD-MM-YYYY'), 185, 'M', 'España', 'Futbol');
--2
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0002', 'Diego Lopez', to_date('03-11-1981', 'DD-MM-YYYY'), 195, 'M', 'España', 'Futbol');
--3
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0003', 'Fernando Amorebieta', to_date('21-03-1985', 'DD-MM-YYYY'), 190, 'M', 'Venezuela', 'Futbol');
--4
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0004', 'Ruud Van Nistelrooy', to_date('01-07-1976', 'DD-MM-YYYY'), 188, 'M', 'Holanda', 'Futbol');
--5
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0005', 'David Luiz', to_date('22-04-1987', 'DD-MM-YYYY'), 189, 'M', 'Brasil', 'Futbol');
--6
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0006', 'Neymar Jr', to_date('05-02-1992', 'DD-MM-YYYY'), 175, 'M', 'Brasil', 'Futbol');
--7
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0007', 'Robinho', to_date('25-01-1984', 'DD-MM-YYYY'), 172, 'M', 'Brasil', 'Futbol');
--8
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0008', 'Javier Hernandez', to_date('01-06-1988', 'DD-MM-YYYY'), 177, 'M', 'Mexico', 'Futbol');
--9
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0009', 'Giovani Dos Santos', to_date('11-05-1989', 'DD-MM-YYYY'), 174, 'M', 'Mexico', 'Futbol');
--10
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0010', 'Rafael Marquez', to_date('13-02-1979', 'DD-MM-YYYY'), 182, 'M', 'Mexico', 'Futbol');
--11
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0011', 'Pedro Rodriguez', to_date('28-07-1987', 'DD-MM-YYYY'), 169, 'M', 'España', 'Futbol');
--12
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0012', 'Arjen Robben', to_date('23-01-1984', 'DD-MM-YYYY'), 180, 'M', 'Holanda', 'Futbol');
--13
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0013', 'Jasper Cillessen', to_date('22-04-1989', 'DD-MM-YYYY'), 188, 'M', 'Holanda', 'Futbol');
--14
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0014', 'Wesley Sneijder', to_date('09-06-1984', 'DD-MM-YYYY'), 170, 'M', 'Holanda', 'Futbol');
--15
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0015', 'Alexis Sanchez', to_date('19-12-1988', 'DD-MM-YYYY'), 171, 'M', 'Chile', 'Futbol');
--16
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0016', 'James Rodriguez', to_date('12-07-1991', 'DD-MM-YYYY'), 180, 'M', 'Colombia', 'Futbol');
--17
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0017', 'Juan Cuadrado', to_date('26-05-1988', 'DD-MM-YYYY'), 176, 'M', 'Colombia', 'Futbol');
--18
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0018', 'Lionel Messi', to_date('24-06-1987', 'DD-MM-YYYY'), 169, 'M', 'Argentina', 'Futbol');
--19
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0019', 'Philipp Lahm', to_date('11-11-1983', 'DD-MM-YYYY'), 170, 'M', 'Alemania', 'Futbol');
--20
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0020', 'Mario Gotze', to_date('03-06-1992', 'DD-MM-YYYY'), 176, 'M', 'Alemania', 'Futbol');
-- Futbol Femenino
--21
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0001', 'Madeline Gier', to_date('28-04-1996', 'DD-MM-YYYY'), 161, 'F', 'Alemania', 'Futbol');
--22
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0002', 'Linda Dallman', to_date('02-09-1994', 'DD-MM-YYYY'), 158, 'F', 'Alemania', 'Futbol');
--23
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0003', 'Sara Daebritz', to_date('15-02-1995', 'DD-MM-YYYY'), 171, 'F', 'Alemania', 'Futbol');
--24
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0004', 'Lena Petermann', to_date('05-02-1994', 'DD-MM-YYYY'), 176, 'F', 'Alemania', 'Futbol');
--25
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0005', 'Asisat Oshoala', to_date('09-10-1994', 'DD-MM-YYYY'), 178, 'F', 'Nigeria', 'Futbol');
--26
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0006', 'Uchechi Sunday', to_date('09-09-1994', 'DD-MM-YYYY'), 182, 'F', 'Nigeria', 'Futbol');
--27
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0007', 'Jiroro Idike', to_date('07-06-1996', 'DD-MM-YYYY'), 151, 'F', 'Nigeria', 'Futbol');
--28
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0008', 'Nicole', to_date('14-08-1995', 'DD-MM-YYYY'), 179, 'F', 'Brasil', 'Futbol');
--29
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0009', 'Duda', to_date('18-07-1995', 'DD-MM-YYYY'), 174, 'F', 'Brasil', 'Futbol');
--30
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0010', 'Andressa', to_date('01-05-1995', 'DD-MM-YYYY'), 161, 'F', 'Brasil', 'Futbol');
--31
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0011', 'Paulina Solis', to_date('13-03-1996', 'DD-MM-YYYY'), 177, 'F', 'Mexico', 'Futbol');
--32
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0012', 'Carolina Jaramillo', to_date('19-03-1994', 'DD-MM-YYYY'), 166, 'F', 'Mexico', 'Futbol');
--33
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0013', 'Charlotte Saint Sans', to_date('20-05-1995', 'DD-MM-YYYY'), 180, 'F', 'Francia', 'Futbol');
--34
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0014', 'Mylaine Tarrieu', to_date('03-01-1995', 'DD-MM-YYYY'), 155, 'F', 'Francia', 'Futbol');
--35
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0015', 'Ashley Lawrence', to_date('11-06-1995', 'DD-MM-YYYY'), 163, 'F', 'Canada', 'Futbol');
--36
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0016', 'Emma Fletcher', to_date('04-02-1995', 'DD-MM-YYYY'), 152, 'F', 'Canada', 'Futbol');
--37
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0017', 'Karen Hermosilla', to_date('07-08-1995', 'DD-MM-YYYY'), 156, 'F', 'Paraguay', 'Futbol');
--38
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0018', 'Belen Benitez', to_date('18-12-1995', 'DD-MM-YYYY'), 155, 'F', 'Paraguay', 'Futbol');
--39
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0019', 'Tania Espinola', to_date('14-10-1996', 'DD-MM-YYYY'), 166, 'F', 'Paraguay', 'Futbol');
--40
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0020', 'Rosalia Godoy', to_date('30-06-1995', 'DD-MM-YYYY'), 167, 'F', 'Paraguay', 'Futbol');
-- Baloncesto Masculino
--41
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0001', 'Thomas Abercrombie', to_date('05-07-1987', 'DD-MM-YYYY'), 198, 'M', 'Nueva Zelanda', 'Baloncesto');
--42
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0002', 'Anthony Davis', to_date('11-03-1993', 'DD-MM-YYYY'), 208, 'M', 'Estados Unidos', 'Baloncesto');
--43
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0003', 'James Harden', to_date('26-08-1989', 'DD-MM-YYYY'), 195, 'M', 'Estados Unidos', 'Baloncesto');
--44
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0004', 'Klay Thompson', to_date('08-02-1990', 'DD-MM-YYYY'), 200, 'M', 'Estados Unidos', 'Baloncesto');
--45
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0005', 'Pau Gasol', to_date('06-07-1980', 'DD-MM-YYYY'), 214, 'M', 'España', 'Baloncesto');
--46
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0006', 'Marc Gasol', to_date('29-01-1985', 'DD-MM-YYYY'), 215, 'M', 'España', 'Baloncesto');
--47
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0007', 'Sergio Llull', to_date('15-11-1987', 'DD-MM-YYYY'), 190, 'M', 'España', 'Baloncesto');
--48
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0008', 'Michael Jordan', to_date('17-02-1963', 'DD-MM-YYYY'), 198, 'M', 'Estados Unidos', 'Baloncesto');
--49
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0009', 'Facundo Campazzo', to_date('23-03-1991', 'DD-MM-YYYY'), 181, 'M', 'Argentina', 'Baloncesto');
--50
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0010', 'Marcos Mata', to_date('01-08-1986', 'DD-MM-YYYY'), 201, 'M', 'Argentina', 'Baloncesto');
-- Baloncesto Femenino
--51
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0001', 'Tamara Tatham', to_date('19-08-1985', 'DD-MM-YYYY'), 182, 'F', 'Canada', 'Baloncesto');
--52
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0002', 'Krysten Boogaard', to_date('18-02-1988', 'DD-MM-YYYY'), 195, 'F', 'Canada', 'Baloncesto');
--53
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0003', 'Michelle Plouffe', to_date('15-09-1992', 'DD-MM-YYYY'), 190, 'F', 'Canada', 'Baloncesto');
--54
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0004', 'Lucila Pascua', to_date('21-03-1983', 'DD-MM-YYYY'), 196, 'F', 'España', 'Baloncesto');
--55
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0005', 'Silvia Dominguez', to_date('31-01-1987', 'DD-MM-YYYY'), 167, 'F', 'España', 'Baloncesto');
--56
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0006', 'Leticia Romero', to_date('28-05-1995', 'DD-MM-YYYY'), 174, 'F', 'España', 'Baloncesto');
--57
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0007', 'Brittney Griner', to_date('18-10-1990', 'DD-MM-YYYY'), 203, 'F', 'Estados Unidos', 'Baloncesto');
--58
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0008', 'Tina Charles', to_date('05-12-1988', 'DD-MM-YYYY'), 193, 'F', 'Estados Unidos', 'Baloncesto');
--59
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0009', 'Angel Mccoughtry', to_date('10-09-1986', 'DD-MM-YYYY'), 185, 'F', 'Estados Unidos', 'Baloncesto');
--60
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0010', 'Odyssey Sims', to_date('13-07-1992', 'DD-MM-YYYY'), 172, 'F', 'Estados Unidos', 'Baloncesto');
-- Beisbol Masculino
--61
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0001', 'Bob Abreu', to_date('11-03-1974', 'DD-MM-YYYY'), 183, 'M', 'Venezuela', 'Beisbol');
--62
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0002', 'Gerardo Parra', to_date('06-05-1987', 'DD-MM-YYYY'), 180, 'M', 'Venezuela', 'Beisbol');
--63
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0003', 'Mario Lisson', to_date('31-05-1984', 'DD-MM-YYYY'), 188, 'M', 'Venezuela', 'Beisbol');
--64
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0004', 'Miguel Cabrera', to_date('18-04-1983', 'DD-MM-YYYY'), 193, 'M', 'Venezuela', 'Beisbol');
--65
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0005', 'Pablo Sandoval', to_date('11-08-1986', 'DD-MM-YYYY'), 180, 'M', 'Venezuela', 'Beisbol');
--66
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0006', 'Marco Scutaro', to_date('30-10-1975', 'DD-MM-YYYY'), 178, 'M', 'Venezuela', 'Beisbol');
--67
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0007', 'Lorenzo Barcelo', to_date('10-08-1977', 'DD-MM-YYYY'), 193, 'M', 'Republica Dominicana', 'Beisbol');
--68
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0008', 'Miguel Tejada', to_date('25-05-1974', 'DD-MM-YYYY'), 175, 'M', 'Republica Dominicana', 'Beisbol');
--69
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0009', 'Frank Morejon', to_date('25-01-1986', 'DD-MM-YYYY'), 173, 'M', 'Cuba', 'Beisbol');
--70
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0010', 'Raul Gonzalez', to_date('22-07-1987', 'DD-MM-YYYY'), 188, 'M', 'Cuba', 'Beisbol');
-- Tenis Masculino
--71
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0001', 'Novak Djokovic', to_date('22-05-1987', 'DD-MM-YYYY'), 188, 'M', 'Yugoslavia', 'Tenis');
--72
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0002', 'Rafael Nadal', to_date('03-06-1986', 'DD-MM-YYYY'), 185, 'M', 'España', 'Tenis');
--73
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0003', 'Roger Federer', to_date('08-08-1981', 'DD-MM-YYYY'), 185, 'M', 'Suiza', 'Tenis');
--74
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0004', 'Stan Wawrinka', to_date('28-03-1985', 'DD-MM-YYYY'), 183, 'M', 'Suiza', 'Tenis');
--75
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0005', 'Key Nishikori', to_date('29-12-1989', 'DD-MM-YYYY'), 178, 'M', 'Japon', 'Tenis');
--76
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0006', 'Juan Del Potro', to_date('23-09-1988', 'DD-MM-YYYY'), 198, 'M', 'Argentina', 'Tenis');
-- Tenis Femenino
--77
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0001', 'Simona Halep', to_date('27-09-1991', 'DD-MM-YYYY'), 168, 'F', 'Romania', 'Tenis');
--78
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0002', 'Maria Sharapova', to_date('19-04-1987', 'DD-MM-YYYY'), 188, 'F', 'Rusia', 'Tenis');
--79
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0003', 'Ana Ivanovic', to_date('06-11-1987', 'DD-MM-YYYY'), 184, 'F', 'Serbia', 'Tenis');
--80
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0004', 'Angelique Kerber', to_date('18-06-1988', 'DD-MM-YYYY'), 173, 'F', 'Alemania', 'Tenis');
--81
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0005', 'Eugenie Bouchard', to_date('25-02-1994', 'DD-MM-YYYY'), 178, 'F', 'Canada', 'Tenis');
--82
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0006', 'Serena Willians', to_date('26-09-1981', 'DD-MM-YYYY'), 175, 'F', 'Estados Unidos', 'Tenis');
-- Waterpolo Masculino
--83
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watm0001', 'Manel Estiarte', to_date('26-10-1961', 'DD-MM-YYYY'), 176, 'M', 'España', 'Waterpolo');
--84
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watm0002', 'Tony Azevedo', to_date('21-11-1981', 'DD-MM-YYYY'), 185, 'M', 'Brasil', 'Waterpolo');
-- Waterpolo Femenino
--85
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watf0001', 'Maggie Steffens', to_date('04-06-1993', 'DD-MM-YYYY'), 175, 'F', 'Estados Unidos', 'Waterpolo');
--86
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watf0002', 'Brenda Villa', to_date('18-04-1980', 'DD-MM-YYYY'), 163, 'F', 'Estados Unidos', 'Waterpolo');
--87
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watf0003', 'Elizabeth Armstrong', to_date('31-01-1983', 'DD-MM-YYYY'), 188, 'F', 'Estados Unidos', 'Waterpolo');
--88
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watf0004', 'Rowena Webster', to_date('27-01-1987', 'DD-MM-YYYY'), 178, 'F', 'Australia', 'Waterpolo');
-- Natacion Masculino
--89
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0001', 'Michael Phelps', to_date('30-06-1985', 'DD-MM-YYYY'), 193, 'M', 'Estados Unidos', 'Natacion');
--90
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0002', 'Ian Thorpe', to_date('13-10-1982', 'DD-MM-YYYY'), 195, 'M', 'Australia', 'Natacion');
--91
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0003', 'Aleksandr Popov', to_date('16-11-1971', 'DD-MM-YYYY'), 200, 'M', 'Rusia', 'Natacion');
--92
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0004', 'Pieter van den Hoogenband', to_date('14-03-1978', 'DD-MM-YYYY'), 193, 'M', 'Holanda', 'Natacion');
--93
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0005', 'Grant Hackett', to_date('09-05-1980', 'DD-MM-YYYY'), 198, 'M', 'Australia', 'Natacion');
--94
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0006', 'Ryan Steven Lochte', to_date('03-08-1984', 'DD-MM-YYYY'), 188, 'M', 'Estados Unidos', 'Natacion');
--95
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0007', 'Matt Biondi', to_date('08-10-1965', 'DD-MM-YYYY'), 201, 'M', 'Estados Unidos', 'Natacion');
-- Natacion Femenino
--96
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natf0001', 'Krisztina Egerszegi', to_date('16-08-1974', 'DD-MM-YYYY'), 174, 'F', 'Hungria', 'Natacion');
--97
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natf0002', 'Jennifer Beth Thompson', to_date('26-02-1973', 'DD-MM-YYYY'), 178, 'F', 'Estados Unidos', 'Natacion');
--98
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natf0003', 'Amy Van Dyken', to_date('15-02-1973', 'DD-MM-YYYY'), 183, 'F', 'Estados Unidos', 'Natacion');
--99
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natf0004', 'Inge de Bruijn', to_date('24-08-1973', 'DD-MM-YYYY'), 178, 'F', 'Holanda', 'Natacion');
--100
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natf0005', 'Yana Klochkova', to_date('07-08-1982', 'DD-MM-YYYY'), 182, 'F', 'Ucrania', 'Natacion');
-- Voleibol Masculino
--101
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volm0001', 'Dante Amaral', to_date('30-09-1980', 'DD-MM-YYYY'), 201, 'M', 'Brasil', 'Voleibol');
--102
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volm0002', 'Gilberto Filho', to_date('23-12-1976', 'DD-MM-YYYY'), 192, 'M', 'Brasil', 'Voleibol');
--103
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volm0003', 'Hector Soto', to_date('20-06-1978', 'DD-MM-YYYY'), 197, 'M', 'Puerto Rico', 'Voleibol');
--104
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volm0004', 'Clayton Stanley', to_date('20-01-1978', 'DD-MM-YYYY'), 205, 'M', 'Estados Unidos', 'Voleibol');
-- Voleibol Femenino
--105
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volf0001', 'Angela Leyva', to_date('22-11-1996', 'DD-MM-YYYY'), 182, 'F', 'Peru', 'Voleibol');
--106
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volf0002', 'Aurea Cruz', to_date('10-01-1982', 'DD-MM-YYYY'), 180, 'F', 'Puerto Rico', 'Voleibol');
--107
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volf0003', 'Gabriela Perez', to_date('10-07-1968', 'DD-MM-YYYY'), 194, 'F', 'Peru', 'Voleibol');
--108
INSERT INTO test_Sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volf0004', 'Susana Rodriguez', to_date('16-08-1976', 'DD-MM-YYYY'), 187, 'F', 'España', 'Voleibol');
