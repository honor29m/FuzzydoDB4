-- Ejemplo de Pokemon del grupo de desempeño
CREATE FUZZY DOMAIN tipos_pokemon AS VALUES ('Planta','Fuego','Agua','Electrico') SIMILARITY {('Planta','Fuego')/0.4,('Planta','Agua')/0.7,('Fuego','Electrico')/0.8,('Agua','Electrico')/0.6};
CREATE FUZZY DOMAIN alturas_pokemon AS POSSIBILITY DISTRIBUTION ON INTEGER;
CREATE TABLE pokemon (Nombre VARCHAR NOT NULL PRIMARY KEY, Altura alturas_pokemon, tipo tipos_pokemon);
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Pichu', {f(5,10,15,20)}, 'Electrico');
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Pikachu', {f(5,15,25,35)}, 'Electrico');
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Charmander', {f(20,30,50,60)}, 'Fuego');
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Squirtle', {f(10,15,35,50)}, 'Agua');
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Bulbasaur', {f(10,20,30,40)}, 'Planta');
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Blastoise', {f(90,120,150,180)}, 'Agua');
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Venusaur', {f(80,100,140,160)}, 'Planta');
INSERT INTO pokemon(nombre, altura, tipo) VALUES ('Charizard', {f(100,130,170,200)}, 'Fuego');

-- ORDER BY, GROUP BY tipo2
SELECT nombre, altura FROM pokemon ORDER BY altura;
SELECT altura, count(*) FROM pokemon GROUP BY altura ORDER BY altura;

-- ORDER BY, GROUP BY tipo3
SELECT nombre, tipo FROM pokemon ORDER BY tipo STARTING FROM 'Electrico';
SELECT tipo, count(*) FROM pokemon GROUP BY SIMILAR tipo;

-- Query Mixto
SELECT nombre, altura, tipo FROM pokemon ORDER BY tipo STARTING FROM 'Electrico';
