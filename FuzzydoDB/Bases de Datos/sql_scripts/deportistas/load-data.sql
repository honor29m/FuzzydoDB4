-- Definicion de dominio tipo 3 
INSERT INTO information_schema_fuzzy.domains(
	domain_id
	,table_schema
	,domain_name
)	VALUES
	
	(1,'test_sports','deporte')
;

-- Definicion de etiquetas tipo 3
INSERT INTO information_schema_fuzzy.labels(
	label_id
	,domain_id
	,label_name
)	VALUES
	
	(1,1,'Futbol')
	,(2,1,'Beisbol')
	,(3,1,'Baloncesto')
	,(4,1,'Tenis')
	,(5,1,'Natacion')
	,(6,1,'Waterpolo')
	,(7,1,'Voleibol')
;

-- Definicion de relacion de similitud tipo 3
INSERT INTO information_schema_fuzzy.similarities(
	label1_id
	,label2_id
	,value
	,derivated
)	VALUES
	 (1,1,1,true)
	,(1,2,0.4,false)
	,(1,3,0.6,false)
	,(1,4,0,false)      --
	,(1,5,0,false)      --
	,(1,6,0.7,false)
	,(1,7,0.5,false)
	,(2,1,0.4,true)
	,(2,2,1,true)
	,(2,3,0.5,false)
	,(2,4,0.1,false)
	,(2,5,0,false)      --
	,(2,6,0,false)      --
	,(2,7,0,false)      --
	,(3,1,0.6,true)
	,(3,2,0.5,true)
	,(3,3,1,true)
	,(3,4,0,false)      --
	,(3,5,0,false)      --
	,(3,6,0.2,false)
	,(3,7,0.7,false)
	,(4,1,0,true)      --
	,(4,2,0.1,true)
	,(4,3,0,true)      --
	,(4,4,1,true)
	,(4,5,0,false)      --
	,(4,6,0,false)      --
	,(4,7,0.3,false)
	,(5,1,0,true)      --
	,(5,2,0,true)      --
	,(5,3,0,true)      --
	,(5,4,0,true)      --
	,(5,5,1,true)
	,(5,6,0.6,false)
	,(5,7,0,false)      --
	,(6,1,0.7,true)
	,(6,2,0,true)      --
	,(6,3,0.2,true)
	,(6,4,0,true)      --
	,(6,5,0.6,true)
	,(6,6,1,true)
	,(6,7,0.5,false)
	,(7,1,0.5,true)
	,(7,2,0,true)      --
	,(7,3,0.7,true)
	,(7,4,0.3,true)
	,(7,5,0,true)      --
	,(7,6,0.5,true)
	,(7,7,1,true)
;

-- Definicion de las columnas tipo 3
INSERT INTO information_schema_fuzzy.columns(
	table_schema
	,table_name
	,column_name
	,domain_id
)	VALUES
	('test_sports','deportistas','deporte', 1)
;


-- Futbol Masculino
--1
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0001', 'Iker Casillas', to_date('20-05-1981', 'DD-MM-YYYY'), 185, 'M', 'España', 1);
--2
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0002', 'Diego Lopez', to_date('03-11-1981', 'DD-MM-YYYY'), 195, 'M', 'España', 1);
--3
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0003', 'Fernando Amorebieta', to_date('21-03-1985', 'DD-MM-YYYY'), 190, 'M', 'Venezuela', 1);
--4
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0004', 'Ruud Van Nistelrooy', to_date('01-07-1976', 'DD-MM-YYYY'), 188, 'M', 'Holanda', 1);
--5
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0005', 'David Luiz', to_date('22-04-1987', 'DD-MM-YYYY'), 189, 'M', 'Brasil', 1);
--6
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0006', 'Neymar Jr', to_date('05-02-1992', 'DD-MM-YYYY'), 175, 'M', 'Brasil', 1);
--7
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0007', 'Robinho', to_date('25-01-1984', 'DD-MM-YYYY'), 172, 'M', 'Brasil', 1);
--8
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0008', 'Javier Hernandez', to_date('01-06-1988', 'DD-MM-YYYY'), 177, 'M', 'Mexico', 1);
--9
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0009', 'Giovani Dos Santos', to_date('11-05-1989', 'DD-MM-YYYY'), 174, 'M', 'Mexico', 1);
--10
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0010', 'Rafael Marquez', to_date('13-02-1979', 'DD-MM-YYYY'), 182, 'M', 'Mexico', 1);
--11
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0011', 'Pedro Rodriguez', to_date('28-07-1987', 'DD-MM-YYYY'), 169, 'M', 'España', 1);
--12
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0012', 'Arjen Robben', to_date('23-01-1984', 'DD-MM-YYYY'), 180, 'M', 'Holanda', 1);
--13
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0013', 'Jasper Cillessen', to_date('22-04-1989', 'DD-MM-YYYY'), 188, 'M', 'Holanda', 1);
--14
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0014', 'Wesley Sneijder', to_date('09-06-1984', 'DD-MM-YYYY'), 170, 'M', 'Holanda', 1);
--15
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0015', 'Alexis Sanchez', to_date('19-12-1988', 'DD-MM-YYYY'), 171, 'M', 'Chile', 1);
--16
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0016', 'James Rodriguez', to_date('12-07-1991', 'DD-MM-YYYY'), 180, 'M', 'Colombia', 1);
--17
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0017', 'Juan Cuadrado', to_date('26-05-1988', 'DD-MM-YYYY'), 176, 'M', 'Colombia', 1);
--18
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0018', 'Lionel Messi', to_date('24-06-1987', 'DD-MM-YYYY'), 169, 'M', 'Argentina', 1);
--19
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0019', 'Philipp Lahm', to_date('11-11-1983', 'DD-MM-YYYY'), 170, 'M', 'Alemania', 1);
--20
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futm0020', 'Mario Gotze', to_date('03-06-1992', 'DD-MM-YYYY'), 176, 'M', 'Alemania', 1);
-- Futbol Femenino
--21
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0001', 'Madeline Gier', to_date('28-04-1996', 'DD-MM-YYYY'), 161, 'F', 'Alemania', 1);
--22
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0002', 'Linda Dallman', to_date('02-09-1994', 'DD-MM-YYYY'), 158, 'F', 'Alemania', 1);
--23
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0003', 'Sara Daebritz', to_date('15-02-1995', 'DD-MM-YYYY'), 171, 'F', 'Alemania', 1);
--24
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0004', 'Lena Petermann', to_date('05-02-1994', 'DD-MM-YYYY'), 176, 'F', 'Alemania', 1);
--25
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0005', 'Asisat Oshoala', to_date('09-10-1994', 'DD-MM-YYYY'), 178, 'F', 'Nigeria', 1);
--26
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0006', 'Uchechi Sunday', to_date('09-09-1994', 'DD-MM-YYYY'), 182, 'F', 'Nigeria', 1);
--27
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0007', 'Jiroro Idike', to_date('07-06-1996', 'DD-MM-YYYY'), 151, 'F', 'Nigeria', 1);
--28
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0008', 'Nicole', to_date('14-08-1995', 'DD-MM-YYYY'), 179, 'F', 'Brasil', 1);
--29
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0009', 'Duda', to_date('18-07-1995', 'DD-MM-YYYY'), 174, 'F', 'Brasil', 1);
--30
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0010', 'Andressa', to_date('01-05-1995', 'DD-MM-YYYY'), 161, 'F', 'Brasil', 1);
--31
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0011', 'Paulina Solis', to_date('13-03-1996', 'DD-MM-YYYY'), 177, 'F', 'Mexico', 1);
--32
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0012', 'Carolina Jaramillo', to_date('19-03-1994', 'DD-MM-YYYY'), 166, 'F', 'Mexico', 1);
--33
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0013', 'Charlotte Saint Sans', to_date('20-05-1995', 'DD-MM-YYYY'), 180, 'F', 'Francia', 1);
--34
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0014', 'Mylaine Tarrieu', to_date('03-01-1995', 'DD-MM-YYYY'), 155, 'F', 'Francia', 1);
--35
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0015', 'Ashley Lawrence', to_date('11-06-1995', 'DD-MM-YYYY'), 163, 'F', 'Canada', 1);
--36
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0016', 'Emma Fletcher', to_date('04-02-1995', 'DD-MM-YYYY'), 152, 'F', 'Canada', 1);
--37
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0017', 'Karen Hermosilla', to_date('07-08-1995', 'DD-MM-YYYY'), 156, 'F', 'Paraguay', 1);
--38
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0018', 'Belen Benitez', to_date('18-12-1995', 'DD-MM-YYYY'), 155, 'F', 'Paraguay', 1);
--39
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0019', 'Tania Espinola', to_date('14-10-1996', 'DD-MM-YYYY'), 166, 'F', 'Paraguay', 1);
--40
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('futf0020', 'Rosalia Godoy', to_date('30-06-1995', 'DD-MM-YYYY'), 167, 'F', 'Paraguay', 1);
-- Baloncesto Masculino
--41
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0001', 'Thomas Abercrombie', to_date('05-07-1987', 'DD-MM-YYYY'), 198, 'M', 'Nueva Zelanda', 3);
--42
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0002', 'Anthony Davis', to_date('11-03-1993', 'DD-MM-YYYY'), 208, 'M', 'Estados Unidos', 3);
--43
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0003', 'James Harden', to_date('26-08-1989', 'DD-MM-YYYY'), 195, 'M', 'Estados Unidos', 3);
--44
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0004', 'Klay Thompson', to_date('08-02-1990', 'DD-MM-YYYY'), 200, 'M', 'Estados Unidos', 3);
--45
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0005', 'Pau Gasol', to_date('06-07-1980', 'DD-MM-YYYY'), 214, 'M', 'España', 3);
--46
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0006', 'Marc Gasol', to_date('29-01-1985', 'DD-MM-YYYY'), 215, 'M', 'España', 3);
--47
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0007', 'Sergio Llull', to_date('15-11-1987', 'DD-MM-YYYY'), 190, 'M', 'España', 3);
--48
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0008', 'Michael Jordan', to_date('17-02-1963', 'DD-MM-YYYY'), 198, 'M', 'Estados Unidos', 3);
--49
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0009', 'Facundo Campazzo', to_date('23-03-1991', 'DD-MM-YYYY'), 181, 'M', 'Argentina', 3);
--50
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balm0010', 'Marcos Mata', to_date('01-08-1986', 'DD-MM-YYYY'), 201, 'M', 'Argentina', 3);
-- Baloncesto Femenino
--51
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0001', 'Tamara Tatham', to_date('19-08-1985', 'DD-MM-YYYY'), 182, 'F', 'Canada', 3);
--52
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0002', 'Krysten Boogaard', to_date('18-02-1988', 'DD-MM-YYYY'), 195, 'F', 'Canada', 3);
--53
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0003', 'Michelle Plouffe', to_date('15-09-1992', 'DD-MM-YYYY'), 190, 'F', 'Canada', 3);
--54
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0004', 'Lucila Pascua', to_date('21-03-1983', 'DD-MM-YYYY'), 196, 'F', 'España', 3);
--55
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0005', 'Silvia Dominguez', to_date('31-01-1987', 'DD-MM-YYYY'), 167, 'F', 'España', 3);
--56
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0006', 'Leticia Romero', to_date('28-05-1995', 'DD-MM-YYYY'), 174, 'F', 'España', 3);
--57
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0007', 'Brittney Griner', to_date('18-10-1990', 'DD-MM-YYYY'), 203, 'F', 'Estados Unidos', 3);
--58
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0008', 'Tina Charles', to_date('05-12-1988', 'DD-MM-YYYY'), 193, 'F', 'Estados Unidos', 3);
--59
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0009', 'Angel Mccoughtry', to_date('10-09-1986', 'DD-MM-YYYY'), 185, 'F', 'Estados Unidos', 3);
--60
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('balf0010', 'Odyssey Sims', to_date('13-07-1992', 'DD-MM-YYYY'), 172, 'F', 'Estados Unidos', 3);
-- Beisbol Masculino
--61
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0001', 'Bob Abreu', to_date('11-03-1974', 'DD-MM-YYYY'), 183, 'M', 'Venezuela', 2);
--62
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0002', 'Gerardo Parra', to_date('06-05-1987', 'DD-MM-YYYY'), 180, 'M', 'Venezuela', 2);
--63
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0003', 'Mario Lisson', to_date('31-05-1984', 'DD-MM-YYYY'), 188, 'M', 'Venezuela', 2);
--64
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0004', 'Miguel Cabrera', to_date('18-04-1983', 'DD-MM-YYYY'), 193, 'M', 'Venezuela', 2);
--65
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0005', 'Pablo Sandoval', to_date('11-08-1986', 'DD-MM-YYYY'), 180, 'M', 'Venezuela', 2);
--66
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0006', 'Marco Scutaro', to_date('30-10-1975', 'DD-MM-YYYY'), 178, 'M', 'Venezuela', 2);
--67
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0007', 'Lorenzo Barcelo', to_date('10-08-1977', 'DD-MM-YYYY'), 193, 'M', 'Republica Dominicana', 2);
--68
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0008', 'Miguel Tejada', to_date('25-05-1974', 'DD-MM-YYYY'), 175, 'M', 'Republica Dominicana', 2);
--69
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0009', 'Frank Morejon', to_date('25-01-1986', 'DD-MM-YYYY'), 173, 'M', 'Cuba', 2);
--70
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('beim0010', 'Raul Gonzalez', to_date('22-07-1987', 'DD-MM-YYYY'), 188, 'M', 'Cuba', 2);
-- Tenis Masculino
--71
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0001', 'Novak Djokovic', to_date('22-05-1987', 'DD-MM-YYYY'), 188, 'M', 'Yugoslavia', 4);
--72
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0002', 'Rafael Nadal', to_date('03-06-1986', 'DD-MM-YYYY'), 185, 'M', 'España', 4);
--73
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0003', 'Roger Federer', to_date('08-08-1981', 'DD-MM-YYYY'), 185, 'M', 'Suiza', 4);
--74
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0004', 'Stan Wawrinka', to_date('28-03-1985', 'DD-MM-YYYY'), 183, 'M', 'Suiza', 4);
--75
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0005', 'Key Nishikori', to_date('29-12-1989', 'DD-MM-YYYY'), 178, 'M', 'Japon', 4);
--76
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenm0006', 'Juan Del Potro', to_date('23-09-1988', 'DD-MM-YYYY'), 198, 'M', 'Argentina', 4);
-- Tenis Femenino
--77
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0001', 'Simona Halep', to_date('27-09-1991', 'DD-MM-YYYY'), 168, 'F', 'Romania', 4);
--78
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0002', 'Maria Sharapova', to_date('19-04-1987', 'DD-MM-YYYY'), 188, 'F', 'Rusia', 4);
--79
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0003', 'Ana Ivanovic', to_date('06-11-1987', 'DD-MM-YYYY'), 184, 'F', 'Serbia', 4);
--80
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0004', 'Angelique Kerber', to_date('18-06-1988', 'DD-MM-YYYY'), 173, 'F', 'Alemania', 4);
--81
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0005', 'Eugenie Bouchard', to_date('25-02-1994', 'DD-MM-YYYY'), 178, 'F', 'Canada', 4);
--82
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('tenf0006', 'Serena Willians', to_date('26-09-1981', 'DD-MM-YYYY'), 175, 'F', 'Estados Unidos', 4);
-- Waterpolo Masculino
--83
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watm0001', 'Manel Estiarte', to_date('26-10-1961', 'DD-MM-YYYY'), 176, 'M', 'España', 6);
--84
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watm0002', 'Tony Azevedo', to_date('21-11-1981', 'DD-MM-YYYY'), 185, 'M', 'Brasil', 6);
-- Waterpolo Femenino
--85
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watf0001', 'Maggie Steffens', to_date('04-06-1993', 'DD-MM-YYYY'), 175, 'F', 'Estados Unidos', 6);
--86
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watf0002', 'Brenda Villa', to_date('18-04-1980', 'DD-MM-YYYY'), 163, 'F', 'Estados Unidos', 6);
--87
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watf0003', 'Elizabeth Armstrong', to_date('31-01-1983', 'DD-MM-YYYY'), 188, 'F', 'Estados Unidos', 6);
--88
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('watf0004', 'Rowena Webster', to_date('27-01-1987', 'DD-MM-YYYY'), 178, 'F', 'Australia', 6);
-- Natacion Masculino
--89
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0001', 'Michael Phelps', to_date('30-06-1985', 'DD-MM-YYYY'), 193, 'M', 'Estados Unidos', 5);
--90
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0002', 'Ian Thorpe', to_date('13-10-1982', 'DD-MM-YYYY'), 195, 'M', 'Australia', 5);
--91
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0003', 'Aleksandr Popov', to_date('16-11-1971', 'DD-MM-YYYY'), 200, 'M', 'Rusia', 5);
--92
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0004', 'Pieter van den Hoogenband', to_date('14-03-1978', 'DD-MM-YYYY'), 193, 'M', 'Holanda', 5);
--93
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0005', 'Grant Hackett', to_date('09-05-1980', 'DD-MM-YYYY'), 198, 'M', 'Australia', 5);
--94
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0006', 'Ryan Steven Lochte', to_date('03-08-1984', 'DD-MM-YYYY'), 188, 'M', 'Estados Unidos', 5);
--95
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natm0007', 'Matt Biondi', to_date('08-10-1965', 'DD-MM-YYYY'), 201, 'M', 'Estados Unidos', 5);
-- Natacion Femenino
--96
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natf0001', 'Krisztina Egerszegi', to_date('16-08-1974', 'DD-MM-YYYY'), 174, 'F', 'Hungria', 5);
--97
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natf0002', 'Jennifer Beth Thompson', to_date('26-02-1973', 'DD-MM-YYYY'), 178, 'F', 'Estados Unidos', 5);
--98
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natf0003', 'Amy Van Dyken', to_date('15-02-1973', 'DD-MM-YYYY'), 183, 'F', 'Estados Unidos', 5);
--99
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natf0004', 'Inge de Bruijn', to_date('24-08-1973', 'DD-MM-YYYY'), 178, 'F', 'Holanda', 5);
--100
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('natf0005', 'Yana Klochkova', to_date('07-08-1982', 'DD-MM-YYYY'), 182, 'F', 'Ucrania', 5);
-- Voleibol Masculino
--101
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volm0001', 'Dante Amaral', to_date('30-09-1980', 'DD-MM-YYYY'), 201, 'M', 'Brasil', 7);
--102
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volm0002', 'Gilberto Filho', to_date('23-12-1976', 'DD-MM-YYYY'), 192, 'M', 'Brasil', 7);
--103
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volm0003', 'Hector Soto', to_date('20-06-1978', 'DD-MM-YYYY'), 197, 'M', 'Puerto Rico', 7);
--104
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volm0004', 'Clayton Stanley', to_date('20-01-1978', 'DD-MM-YYYY'), 205, 'M', 'Estados Unidos', 7);
-- Voleibol Femenino
--105
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volf0001', 'Angela Leyva', to_date('22-11-1996', 'DD-MM-YYYY'), 182, 'F', 'Peru', 7);
--106
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volf0002', 'Aurea Cruz', to_date('10-01-1982', 'DD-MM-YYYY'), 180, 'F', 'Puerto Rico', 7);
--107
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volf0003', 'Gabriela Perez', to_date('10-07-1968', 'DD-MM-YYYY'), 194, 'F', 'Peru', 7);
--108
INSERT INTO test_sports.deportistas(codigo_jugador, nombre, fecha_nac, altura, sexo, pais, deporte) VALUES
	('volf0004', 'Susana Rodriguez', to_date('16-08-1976', 'DD-MM-YYYY'), 187, 'F', 'España', 7);
