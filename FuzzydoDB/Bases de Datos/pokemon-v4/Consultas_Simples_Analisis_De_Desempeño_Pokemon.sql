-- CONSULTAS SIMPLES!!!
-- 1: Consultas ST2 y ST3 (CLAUSULA SELECT)

----1.1: caso no ST2, no ST3:
SELECT nombre FROM pokemon;

----1.2: caso si ST2, no ST3:
SELECT poder_base_estimado FROM pokemon;

----1.3: caso no ST2, si ST3:
SELECT color FROM pokemon;

----1.4: caso si ST2, si ST3:
SELECT poder_base_estimado, color FROM pokemon;

-- 2: Consultas GBT2 y GBT3 (CLAUSULA GROUP BY)

----2.1: caso no GBT2, no GBT3:
SELECT count(*) AS total, region_origen FROM pokemon GROUP BY region_origen;

----2.2: caso si GBT2, no GBT3:
SELECT count(*) AS total, poder_base_estimado FROM pokemon GROUP BY poder_base_estimado;

----2.3: caso no GBT2, si GBT3:
SELECT count(*) AS total, color FROM pokemon GROUP BY SIMILAR color;

----2.4: caso si GBT2, si GBT3:
SELECT count(*) AS total, poder_base_estimado, color FROM pokemon GROUP BY SIMILAR color, poder_base_estimado;

-- 3: Consultas OBT2 y OBT3 (CLAUSULA GROUP BY)

----3.1: caso no OBT2, no OBT3:
SELECT nombre FROM pokemon ORDER BY nombre;

----3.2: caso si OBT2, no OBT3:
SELECT poder_base_estimado FROM pokemon ORDER BY poder_base_estimado;

----3.3: caso no OBT2, si OBT3:
SELECT color FROM pokemon ORDER BY color STARTING FROM 'Amarillo';

----3.4: caso si OBT2, si OBT3:
SELECT poder_base_estimado, color FROM pokemon ORDER BY color STARTING FROM 'Amarillo', poder_base_estimado;

-- 4: Consultas PDB y FDB (POSTGRES DB vs FUZZYDO DB)

----4.1 = 1.1:
SELECT nombre FROM pokemon;

----4.2 = 2.1:
SELECT count(*) AS total, region_origen FROM pokemon GROUP BY region_origen;

----4.3 = 3.1:
SELECT nombre FROM pokemon ORDER BY nombre;
