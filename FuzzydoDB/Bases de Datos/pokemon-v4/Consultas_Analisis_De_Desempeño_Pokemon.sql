-- 1: Consultas ST2 y ST3 (CLAUSULA SELECT)

----1.1: caso no ST2, no ST3:
SELECT nombre, habilidad_1, habilidad_2, habilidad_oculta FROM pokemon WHERE habilidad_1 IN ('Levitación', 'Clorofila', 'Presión') OR (habilidad_2 IS NOT NULL AND habilidad_oculta LIKE 'P%');

----1.2: caso si ST2, no ST3:
SELECT nombre, poder_base_estimado, ratio_de_captura FROM pokemon WHERE habilidad_1 IN ('Levitación', 'Clorofila', 'Presión') OR (habilidad_2 IS NOT NULL AND habilidad_oculta LIKE 'P%');

----1.3: caso no ST2, si ST3:
SELECT nombre, color, tipo_1, tipo_2, grupo_huevo_1, grupo_huevo_2 FROM pokemon WHERE habilidad_1 IN ('Levitación', 'Clorofila', 'Presión') OR (habilidad_2 IS NOT NULL AND habilidad_oculta LIKE 'P%');

----1.4: caso si ST2, si ST3:
SELECT nombre, region_origen, poder_base_estimado, tipo_1, tipo_2 FROM pokemon WHERE habilidad_1 IN ('Levitación', 'Clorofila', 'Presión') OR (habilidad_2 IS NOT NULL AND habilidad_oculta LIKE 'P%');

-- 2: Consultas GBT2 y GBT3 (CLAUSULA GROUP BY)

----2.1: caso no GBT2, no GBT3:
SELECT count(*) AS total, p.region_origen FROM pokemon AS p, mega_pokemon AS m WHERE p.codigo_pokedex_nacional = m.codigo_pokedex_nacional GROUP BY p.region_origen;

----2.2: caso si GBT2, no GBT3:
SELECT count(*) AS total, p.poder_base_estimado FROM pokemon AS p, mega_pokemon AS m WHERE p.codigo_pokedex_nacional = m.codigo_pokedex_nacional GROUP BY p.poder_base_estimado;

----2.3: caso no GBT2, si GBT3:
SELECT count(*) AS total, p.color FROM pokemon AS p, mega_pokemon AS m WHERE p.codigo_pokedex_nacional = m.codigo_pokedex_nacional GROUP BY SIMILAR p.color;

----2.4: caso si GBT2, si GBT3:
SELECT count(*) AS total, p.ratio_de_captura, p.tipo_1 FROM pokemon AS p, mega_pokemon AS m WHERE p.codigo_pokedex_nacional = m.codigo_pokedex_nacional GROUP BY SIMILAR p.tipo_1, p.ratio_de_captura;

-- 3: Consultas OBT2 y OBT3 (CLAUSULA GROUP BY)

----3.1: caso no OBT2, no OBT3:
SELECT nombre, region_origen, habilidad_1, habilidad_2, habilidad_oculta FROM pokemon WHERE region_origen = 'Kanto' OR (region_origen IN ('Johto', 'Kalos') AND habilidad_1 LIKE 'Absorbe%') ORDER BY nombre;

----3.2: caso si OBT2, no OBT3:
SELECT nombre, poder_base_estimado, ratio_de_captura FROM pokemon WHERE region_origen = 'Kanto' OR (region_origen IN ('Johto', 'Kalos') AND habilidad_1 LIKE 'Absorbe%') ORDER BY poder_base_estimado DESC;

----3.3: caso no OBT2, si OBT3:
SELECT nombre, tipo_1, tipo_2, color FROM pokemon WHERE region_origen = 'Kanto' OR (region_origen IN ('Johto', 'Kalos') AND habilidad_1 LIKE 'Absorbe%') ORDER BY tipo_1 STARTING FROM 'Tipo bicho';

----3.4: caso si OBT2, si OBT3:
SELECT nombre, tipo_1, ratio_de_captura FROM pokemon WHERE region_origen = 'Kanto' OR (region_origen IN ('Johto', 'Kalos') AND habilidad_1 LIKE 'Absorbe%') ORDER BY tipo_1 STARTING FROM 'Tipo bicho', ratio_de_captura DESC;



-- 4: Consultas PDB y FDB (POSTGRES DB vs FUZZYDO DB)
----4.1 = 1.1:
SELECT nombre, habilidad_1, habilidad_2, habilidad_oculta FROM pokemon WHERE habilidad_1 IN ('Levitación', 'Clorofila', 'Presión') OR (habilidad_2 IS NOT NULL AND habilidad_oculta LIKE 'P%');

----4.2 = 2.1:
SELECT count(*) AS total, p.region_origen FROM pokemon AS p, mega_pokemon AS m WHERE p.codigo_pokedex_nacional = m.codigo_pokedex_nacional GROUP BY p.region_origen;

----4.3 = 3.1:
SELECT nombre, region_origen, habilidad_1, habilidad_2, habilidad_oculta FROM pokemon WHERE region_origen = 'Kanto' OR (region_origen IN ('Johto', 'Kalos') AND habilidad_1 LIKE 'Absorbe%') ORDER BY nombre;

